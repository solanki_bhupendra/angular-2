export class ProductModel {

    private _productId: number;
    private _productName: string;
    private _productCode: string;
    private _releaseDate: string;
    private _price: number;
    private _description: string;
    private _starRating: number;
    private _imageUrl: string;
    constructor() {
        
    }

    public get ProductId():number{
        return this._productId;
    }

    public set ProductId(productId:number){
         this._productId = productId;
    }

    public get ProductName():string{
        return this._productName;
    }

    public set ProductName(productName:string){
         this._productName = productName;
    }

    public get ProductCode():string{
        return this._productCode;
    }

    public set ProductCode(productCode:string){
         this._productCode = productCode;
    }

    public get ImageUrl():string{
        return this._imageUrl;
    }

    public set ImageUrl(imageUrl:string){
         this._imageUrl = imageUrl;
    }

    public get StarRating():number{
        return this._starRating;
    }

    public set StarRating(starRating:number){
         this._starRating = starRating;
    }


}