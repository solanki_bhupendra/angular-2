import { Component, OnInit } from '@angular/core';

import { IProduct } from './product';
import { ProductService } from './product.service';
import { ProductModel } from "../model/productModel";

@Component({
    selector: 'pm-products',
    templateUrl: 'app/products/product-list.component.html',
    styleUrls: ['app/products/product-list.component.css']
})
export class ProductListComponent implements OnInit {
    pageTitle: string = 'Product List';
    imageWidth: number = 50;
    imageMargin: number = 2;
    showImage: boolean = false;
    listFilter: string;
    errorMessage: string;

    products: IProduct[];
    productModelData:ProductModel[] = [];

    constructor(private _productService: ProductService) {

    }

    toggleImage(): void {
        this.showImage = !this.showImage;
    }

    ngOnInit(): void {
        this._productService.getProducts()
                .subscribe(products => {
                    this.products = products;
                    this.products.forEach(p => {
                        let objModel = new ProductModel();
                        objModel.ProductCode = p.productCode;
                        objModel.ProductId = p.productId;
                        objModel.ProductName = p.productName;
                        objModel.StarRating = p.starRating;
                        this.productModelData.push(objModel)
                       
                    });
                    
                },error => this.errorMessage = <any>error);

                 console.log("forEach",  this.productModelData);
    }

    onRatingClicked(message: string): void {
        this.pageTitle = 'Product List: ' + message;
    }

     deleteRow(evt:any):void{
        console.log("deleteRow");
      if(this.products.length > 2){
            this.products.splice(this.products.length-1, 1);
        }
        
        
    }

    addRow(evt:any):void{
        var self = this;
        var objFields = this.products;
        
        objFields.push(objFields[objFields.length-1]);
        //console.log(arrFields);
       
        objFields.forEach(function (value:any, index:number) {
            if(index > 0){
                //console.log(self.testForReadability(value.id));
                 value["productCode"] = "row_" + (index);
                console.log(value, index);
            }
            
        }); 

        // arrFields.push(arrFields[arrFields.length-1]);
        //  var arrFieldGroup:Array<FormlyFieldConfig> = arrFields[arrFields.length-1]["fieldGroup"];
        //          arrFieldGroup.forEach(function (val:any, indx:number) {
        //             val["key"] = "elem_" + 6 + "_" + (indx+1);
        //             val["templateOptions"].id = "elem_" + 6 + "_" + (indx+1);
        //          });
        console.log(objFields);
    }
}
