import { NgModule }      from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule }      from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent }  from './app.component';
import { InputTextModule
          , ButtonModule
          , DataTableModule
          , DropdownModule
          , CalendarModule
          , DialogModule
          , GrowlModule }  from 'primeng/primeng';

import { PDataTableComponent } from './components/datatable/pdatatable.component';
import { ExpandedDTComponent } from "./components/datatable/expandedDT.component";
import { LazyLoadingComponent } from "./components/datatable/lazyloading.component";
import { CalendarComponent } from './components/calendar/calendar.component';
import { PDataTableService } from './components/datatable/pdatatable.service';
import { DropDownComponent } from "./components/dropdown/dropdown.component";

const appRoutes: Routes = [
  { path: 'expandedDt', component: ExpandedDTComponent },
  { path: 'datatable', component: PDataTableComponent },
  { path: 'lazyloadingDt', component: LazyLoadingComponent },
  { path: 'calender', component: CalendarComponent },
  { path: 'dropdown', component: DropDownComponent },
  { path: '',
    redirectTo: '/PDataTableComponent',
    pathMatch: 'full'
  },
  { path: '**', component: PDataTableComponent }
];

@NgModule({
  imports:      [ BrowserModule
                  , HttpModule
                  ,  RouterModule.forRoot(
                          appRoutes,
                          { enableTracing: true } // <-- debugging purposes only
                        )
                  , BrowserAnimationsModule
                  , FormsModule
                  , InputTextModule
                  , ButtonModule
                  , DataTableModule
                  , DropdownModule
                  , DialogModule
                  , CalendarModule
                  , GrowlModule ],
  declarations: [ AppComponent
                  , PDataTableComponent 
                  , DropDownComponent
                  , CalendarComponent
                  , LazyLoadingComponent
                  , ExpandedDTComponent],
  providers:     [PDataTableService],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
