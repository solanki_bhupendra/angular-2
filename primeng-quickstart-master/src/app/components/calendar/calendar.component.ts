import { Component, OnInit } from '@angular/core';
import { DatePipe } from "@angular/common";

@Component({
    selector: 'datetimepicker',
    templateUrl: 'app/components/calendar/calendar.component.html',
    styleUrls: ['app/components/calendar/calendar.component.css']
})

export class CalendarComponent implements OnInit {
    datePipe = new DatePipe('en-US');
    value: Date;
    constructor() { }

    ngOnInit() { }

    changeDateFormat(datevalue: string): Date {
        let dateSplit = datevalue.split('/');
        let newDate = dateSplit[1] + '/' + dateSplit[0] + '/' + dateSplit[2];
        let changeDate: Date = new Date(newDate);
        return changeDate;
  }

  onDataSelect(event: any){
      //let newData: Date = new Date(event);
    
    console.log(this.datePipe.transform(event, 'dd/MM/yyyy'));
  }

  setCalendarValue(event: any){
    //this.value = new Date(2017,9,5);
    this.value = this.changeDateFormat('23/10/2017');
  }
}