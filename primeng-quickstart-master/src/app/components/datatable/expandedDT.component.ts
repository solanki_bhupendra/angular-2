import { Component, OnInit } from '@angular/core';
import {Car} from '../model/car';
import {Message} from '../model/message';
import {PDataTableService} from './pdatatable.service';
import {SelectItem} from '../model/selectItem';
import {Observable} from 'rxjs/Observable';

class PrimeCar implements Car {
    constructor(public vin?: any, public year?: any, public brand?: any, public color?: any) {}
}
@Component({
    selector: 'expanded-table',
    templateUrl: 'app/components/datatable/expandedDT.component.html'
})

export class ExpandedDTComponent implements OnInit {
    cars: Car[];
    
    cols: any[];
    
    selectedCar: Car;
    
    dialogVisible: boolean;
    
    constructor(private carService: PDataTableService) { }

    ngOnInit() {
        this.carService.getCarsSmall().then(cars => this.cars = cars);
        
        this.cols = [
            {field: 'vin', header: 'Vin'},
            {field: 'year', header: 'Year'},
            {field: 'brand', header: 'Brand'},
            {field: 'color', header: 'Color'}
        ];
    }
    
    showCar(car: Car) {
        console.log("car", car);
        this.selectedCar = car;
        this.dialogVisible = true;
    }
}