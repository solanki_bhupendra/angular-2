import { Component, OnInit, ViewChild } from '@angular/core';
import {Car} from '../model/car';
import {Message} from '../model/message';
import {PDataTableService} from './pdatatable.service';
import {SelectItem} from '../model/selectItem';
import {Observable} from 'rxjs/Observable';
import { DataTable } from 'primeng/components/datatable/datatable';
import { Dropdown } from 'primeng/components/dropdown/dropdown';
class PrimeCar implements Car {
    constructor(public vin?: any, public year?: any, public brand?: any, public color?: any) {}
}

@Component({
    selector: 'pdataTable',
    templateUrl: 'app/components/datatable/pdatatable.component.html',
    styleUrls: ['app/components/datatable/pdatatable.component.css']
})

export class PDataTableComponent implements OnInit {
    @ViewChild(DataTable) dataTableComp: DataTable;
    @ViewChild(Dropdown) dropdownComp: Dropdown;
    cars: Car[];
    msgs: Message[];
    selectedCar: Car;
    brands: SelectItem[];
    colors: SelectItem[];
    selectedColor: string;
    value: Date;
    custFilter: string;
    defaultDDValue:SelectItem;
    private data: Observable<Array<number>>;
    private values: Array<number> = [];
    private anyErrors: boolean;
    private finished: boolean;

    constructor(private pdatatableService: PDataTableService) { }

    ngOnInit() {
        this.pdatatableService.getCarsSmall().then(cars => {
                        this.cars = cars});
       this.brands = [];
        this.brands.push({label: 'All Brands', value: null});
        this.brands.push({label: 'Audi', value: 'Audi'});
        this.brands.push({label: 'BMW', value: 'BMW'});
        this.brands.push({label: 'Fiat', value: 'Fiat'});
        this.brands.push({label: 'Honda', value: 'Honda'});
        this.brands.push({label: 'Jaguar', value: 'Jaguar'});
        this.brands.push({label: 'Mercedes', value: 'Mercedes'});
        this.brands.push({label: 'Renault', value: 'Renault'});
        this.brands.push({label: 'VW', value: 'VW'});
        this.brands.push({label: 'Volvo', value: 'Volvo'});

       this.setDefaultColors();
        
    }

      init() {
      this.data = new Observable(observer => {
          setTimeout(() => {
              observer.add(() => {console.log(445)})
          }, 1000);
          
          setTimeout(() => {
              observer.next(3);
          }, 2000);
          
          setTimeout(() => {
              observer.complete();
          }, 3000);
          
      });
  }

    delete(data: Car): void{
        let newCar: Car = new PrimeCar();
        let index = this.findSelectedCarIndex(data);
        
        //this.msgs = [];
        //this.msgs.push({severity: 'info', summary: 'Car Deleted', detail: this.cars[index].vin + ' - ' + this.cars[index].brand});

        this.cars = this.cars.filter((val, i) => i !== index);

        if (this.cars.length < 10) {
            let newCars = [...this.cars];
            newCars.push(newCar);
            this.cars = newCars;
        }
        
        //this.newCar = null;

        console.log("this.cars", this.cars.length);
    }

    save() {
        let newCars = [...this.cars];
        let newCar: Car;
        for (let i = 0; i < 2; i++) {
            newCar = new PrimeCar();
            newCars.push(newCar);
        }
        this.cars = newCars;
        console.log('length', this.cars.length)
    }

    findSelectedCarIndex(item: Car): number {
        return this.cars.indexOf(item);
    }

    resetFilter(): void{
        this.dataTableComp.reset();
       
        this.dropdownComp.resetFilter();
        this.setDefaultColors();
        this.value = undefined;
        this.custFilter = '';
        this.defaultDDValue = {label: 'All Brands', value: null};
        
    }


    setDefaultColors():void{
         this.colors = [];
        this.colors.push({label: 'Choose', value: null});
        this.colors.push({label: 'White', value: 'White'});
        this.colors.push({label: 'Green', value: 'Green'});
        this.colors.push({label: 'Silver', value: 'Silver'});
        this.colors.push({label: 'Black', value: 'Black'});
        this.colors.push({label: 'Red', value: 'Red'});
        this.colors.push({label: 'Maroon', value: 'Maroon'});
        this.colors.push({label: 'Brown', value: 'Brown'});
        this.colors.push({label: 'Orange', value: 'Orange'});
        this.colors.push({label: 'Blue', value: 'Blue'});

        

        
    }

    setColorValues(event: any): void{
        this.setDefaultColors();
    }

    setValue(event: any): void{
        this.selectedColor = 'Silver';
        // this.colors = [];
        // this.colors.push({label: 'Silver', value: 'Silver'});
    }

    setDateValue(event: any): void{
       this.value = new Date(2016,(2-1),30);
    }

    setYearValue(event: any): void{
       this.custFilter = '2005';
    }
}