import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import {Car} from '../model/car';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class PDataTableService {
    
    constructor(private http: Http) {}

    getCarsSmall() {
        return this.http.get('asests/json/cars-small.json')
                    .toPromise()
                    .then(res => <Car[]> res.json().data)
                    .then(data => { console.log('getCarsSmall', data); return data; });
    }

    getCarsMedium() {
        return this.http.get('asests/json/cars-medium.json')
                    .toPromise()
                    .then(res => <Car[]> res.json().data)
                    .then(data => { console.log(data); return data; });
    }

    getCarsLarge() {
        return this.http.get('asests/json/cars-large.json')
                    .toPromise()
                    .then(res => <Car[]> res.json().data)
                    .then(data => { return data; });
    }
}