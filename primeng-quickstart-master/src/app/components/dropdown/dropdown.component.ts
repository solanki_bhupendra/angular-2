import { Component, OnInit, ViewChild } from '@angular/core';
import {SelectItem} from 'primeng/primeng';
import { Dropdown } from "primeng/components/dropdown/dropdown";

@Component({
    selector: 'dropdown-element',
    templateUrl: 'app/components/dropdown/dropdown.component.html',
    styleUrls: ['app/components/dropdown/dropdown.component.css']
})

export class DropDownComponent implements OnInit {
    @ViewChild(Dropdown) dropDownComponent: Dropdown;
     cities: SelectItem[];

    selectedCity: string;

    constructor() {
        this.setDefaultValue();
    }

    ngOnInit() { }

    setDropDownValue(event: any): void{
       this.selectedCity = 'London';
    }

    onDropDownClick(event: any){
        console.log('======12121');
        
    }

    setDefaultValue():void{
        this.cities = [];
        this.cities.push({label:'Select City', value:null});
        this.cities.push({label:'New York', value: 'New York'});
        this.cities.push({label:'Rome', value: 'Rome'});
        this.cities.push({label:'London', value: 'London'});
        this.cities.push({label:'Istanbul', value: 'Istanbul'});
        this.cities.push({label:'Paris', value: 'Paris'});
    }
}